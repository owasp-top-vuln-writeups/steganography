# Steganography

Steganography is the art, of hiding data in images. 

This is a collection of several tools and process, to assist in steganography related 
investigations.

```ruby 

Easy check list when starting an investigation

strings
exif
binwalk
pngcheck
Explore Colour * Bit Planes
Extract Least Significatn Byte Data
Check RGB Values
steg hide
Out guess
Changing colour palette
```

Steg hide allows data to be hidden, at a more complex level. Commonly requires a password to unhide.

```ruby
steghide extract -sf filename
```
1. Easy Stego, basic tool
    https://www.secsy.net/easy_stegoCTF

2. Online tool manipulate imnage colors and dimensions. 
    https://29a.ch/photo-forensics/#pca

3. https://georgeom.net/StegOnline/upload (CTF Focused)

4. Great Stego Kit here - With GUI Option (Runs on docker)
    https://github.com/DominicBreuker/stego-toolkit

5. Autopsy (Runs on windows)
    https://www.sleuthkit.org/autopsy/
    
6. Great guide with list
    https://0xrick.github.io/lists/stego/
